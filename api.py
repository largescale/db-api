from flask import Flask, jsonify, request
from flask_restful import reqparse, abort, Api, Resource
import mysql.connector
from datetime import datetime
import json
import random
import logging
import os, sys

os.chdir(sys.path[0]) #changes working directory to write the log file in the same folder as the script

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

api = Api(app)

connection_config = {
    'host':'localhost',
    'user':'app',
    'passwd':'SD2019app',
    'database':'dome'
}

def get_connection():
    try:
        mydb = mysql.connector.connect(**connection_config)
        if (mydb.is_connected() is False): #If connection exists leave Try statement
            logging.info("MYSQL: Trying to connect to the database.")
            mydb = mysql.connector.connect(**connection_config)
            logging.info("MYSQL: Database connection was successfull")
    except Exception as err:
        logging.error("MYSQL: {0}".format(err))
        return False
    finally:
        if mydb.is_connected() is True: #If connection exists leave Try statement
            return mydb

#    Previous connection code (tried to maintain the conection always active)
#    global mydb
#    running = True
#    while running:
#        try:
#            if (mydb.is_connected() is False): #If connection exists leave Try statement
#                logging.info("MYSQL: Trying to connect to the database.")
#                mydb = mysql.connector.connect(**connection_config)
#                logging.info("MYSQL: Database connection was successfull")
#        except Exception as err:
#            logging.error("MYSQL: {0}".format(err))
#        finally:
#            if mydb.is_connected() is True: #If connection exists leave Try statement
#                running = False
#                return mydb


def user_exists(myresult):
    if len(myresult) == 0:
        return False

    return True


# Returns the beacon id using its minor and major
def getBeaconID(minor, major):
    myconn = get_connection()

    if myconn.is_connected() is True:
        mycursor = myconn.cursor()

        mycursor.execute("SELECT id FROM beacon WHERE minor = " + str(minor) + " AND major = " + str(major) + " LIMIT 1")

        myresult = mycursor.fetchall()

        return myresult[0][0]
        

#This method is called when a beacon_log is requested, if the visit is ongoing its updated 
# or a new visit is started if the previous post was longer than x minutes ago
def update_visit(beacon_id):
    myconn = get_connection()
    
    if myconn.is_connected() is True:
        mycursor = myconn.cursor()
        
        mycursor.execute("SELECT * FROM visit WHERE end_time is NULL")
        myresult = mycursor.fetchall()

        #if a visit is ongoing, updates it and creates a new one with the zone recieved from the beacon
        if len(myresult) == 1:
            for result in myresult:
                user_id = result[3]
                last_visit_id = result[0]
                last_post_time = result[1]
            
            

            time_difference = datetime.now() - last_post_time

            if (time_difference.seconds <= 60):
                #adds new visit
                zone_id = get_zoneID_with_beaconID(beacon_id)

                insert_query = "INSERT INTO visit(user_id, zone_id) "\
                    "VALUES (%s,%s)"
                insert_val = (user_id, zone_id)
                mycursor.execute(insert_query, insert_val)
                
                #inserts end_time on the previous visit
                end_last_visit(last_visit_id)
                
                myconn.commit()
            else:
                end_last_visit(last_visit_id)
                update_visit(beacon_id)
                myconn.commit()

        else: #else starts a new visit with a new user
            current_time = str(datetime.now().time())
            user_name = "user" + current_time
            user_email = user_name + "@mail.com"
            gender_id = random.randint(1,2)
            birthday = datetime.now()
            country = "Unknown"
            mac_address = current_time

            mycursor.execute("INSERT INTO user(name, email, gender_id, birthday, country, mac_address) "\
                                "VALUES (%s,%s,%s,%s,%s,%s)", (user_name, user_email, gender_id, birthday, country, mac_address))
            
            created_user_id = mycursor.lastrowid

            mycursor.execute("INSERT INTO visit(user_id, zone_id) "\
                                "VALUES (%s,%s)", (created_user_id, get_zoneID_with_beaconID(beacon_id)))
            myconn.commit()


#inserts end_time on the previous visit
def end_last_visit(last_visit_id):
    myconn = get_connection()
    mycursor = myconn.cursor()
    mycursor.execute("UPDATE visit "\
                "SET end_time = CURRENT_TIMESTAMP "\
                "WHERE id = " + str(last_visit_id) + " AND end_time is NULL")
    myconn.commit()


def get_zoneID_with_beaconID(beacon_id):
    myconn = get_connection()
    mycursor = myconn.cursor()
    
    mycursor.execute("SELECT * FROM zone WHERE beacon_id = " + str(beacon_id))
    myresult = mycursor.fetchall()

    for result in myresult:
        zone_id = result[0]

    return zone_id


############################
# User
############################
# shows a single user item
class UserByID(Resource):
    def get(self, user_id):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT u.id AS id, u.name AS name, u.email AS email, g.designation AS gender, "\
            "u.birthday AS birthday, u.country AS country, u.donation AS donation, u.mac_address AS mac_address, u.created_at AS created_at "\
            "FROM user u "\
            "JOIN gender g ON u.gender_id=g.id "\
            "WHERE u.id=" + str(user_id) + ";")
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        if (user_exists(myresult)):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'data': data}
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': "User " + str(user_id) + " doesn't exist."}

        return jsonify(response)

class UserDonation(Resource):
    def put(self):
        data = request.get_json()
        
        missing_values = []

        if 'donation' not in data: missing_values.append("donation")
        if 'user_id' not in data: missing_values.append("user_id")
        
        insert_success = True
        error_message = ""

        if len(missing_values) > 0:
            insert_success = False
            error_message += "MISSING VALUES: " + ', '.join(missing_values)
        else:
            myconn = get_connection()
            mycursor = myconn.cursor()

            sql = "UPDATE user SET donation=%s WHERE id=%s"
            val = (data['donation'], data['user_id'])

            insert_success = True
            error_message = ""

            try:
                mycursor.execute(sql, val)
                myconn.commit()
            except Exception as err:
                insert_success = False
                error_message += "MYSQL: {0}".format(err)

            # get updated user data
            mycursor.execute("SELECT * "\
                                "FROM user u "\
                                "WHERE u.id = " + str(data['user_id']) + ";")

            myresult = mycursor.fetchall()

            row_headers=[x[0] for x in mycursor.description] #this will extract row headers

            response_data=[]
            for result in myresult:
                response_data.append(dict(zip(row_headers,result)))

        if (insert_success):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'data': response_data}
            logging.info("%s record(s) updated.", mycursor.rowcount)
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': error_message}
        
        return jsonify(response)

# UserList
# shows a list of all users, and lets you POST to add new user
class UserList(Resource):
    def get(self):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT u.id AS id, u.name AS name, u.email AS email, g.designation AS gender, "\
            "u.birthday AS birthday, u.country AS country, u.donation AS donation, u.mac_address AS mac_address, u.created_at AS created_at "\
            "FROM user u "\
            "JOIN gender g ON u.gender_id=g.id "\
            "ORDER BY u.id DESC")
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        response = {'success': True,
                    'timestamp': str(datetime.now()),
                    'data': data}

        return jsonify(response)

    # Adds new user
    def post(self):
        data = request.get_json()

        missing_values = []

        if 'name' not in data: missing_values.append("name")
        if 'email' not in data: missing_values.append("email")
        if 'gender_id' not in data: missing_values.append("gender_id")
        if 'birthday' not in data: missing_values.append("birthday")
        if 'country' not in data: missing_values.append("country")
        if 'mac_address' not in data: missing_values.append("mac_address")
        
        insert_success = True
        error_message = ""

        if len(missing_values) > 0:
            insert_success = False
            error_message += "MISSING VALUES: " + ', '.join(missing_values)
        else:
            myconn = get_connection()
            mycursor = myconn.cursor()

            sql = "INSERT INTO user (name, email, gender_id, birthday, country, mac_address) VALUES (%s, %s, %s, %s, %s, %s)"
            val = (data['name'], data['email'], data['gender_id'], data['birthday'], data['country'], data['mac_address'])

            try:
                mycursor.execute(sql, val)
                myconn.commit()
            except Exception as err:
                insert_success = False
                error_message += "MYSQL: {0}".format(err)


        if (insert_success):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'inserted_id': mycursor.lastrowid}
            logging.info("%s record(s) inserted.", mycursor.rowcount)
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': error_message}

        return jsonify(response)


#############################
# Result
#############################
# ResultList
# shows a list of all results, and lets you POST to add new result
class ResultList(Resource):
    def get(self):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT r.id AS id, r.question_id AS question_id, q.question AS question, "\
                            "r.user_id AS user_id, r.score AS score, "\
                            "r.comment AS comment, r.created_at AS created_at "\
                            "FROM result r "\
                            "JOIN question q ON r.question_id=q.id "\
                            "ORDER BY id DESC")

        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        response = {'success': True,
                    'timestamp': str(datetime.now()),
                    'data': data}
        
        return jsonify(response)

    # Adds new result
    def post(self):
        data = request.get_json()

        missing_values = []

        if 'question_id' not in data: missing_values.append("question_id")
        if 'user_id' not in data: missing_values.append("user_id")
        if 'score' not in data: missing_values.append("score")
        
        insert_success = True
        error_message = ""

        if len(missing_values) > 0:
            insert_success = False
            error_message += "MISSING VALUES: " + ', '.join(missing_values)
        else:
            myconn = get_connection()
            mycursor = myconn.cursor()

            comment = ""
            if 'comment' in data:
                comment = str(data['comment'])
            else:
                comment = ""

            sql = "INSERT INTO result (question_id, user_id, score, comment) VALUES (%s, %s, %s, %s)"
            val = (data['question_id'], data['user_id'], data['score'], comment)

            insert_success = True
            error_message = ""

            try:
                mycursor.execute(sql, val)
                myconn.commit()
            except Exception as err:
                insert_success = False
                error_message += "MYSQL: {0}".format(err)


        if (insert_success):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'inserted_id': mycursor.lastrowid}
            logging.info("%s record(s) inserted.", mycursor.rowcount)
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': error_message}

        return jsonify(response)


# shows results filtered by question_id
class ResultByQuestionID(Resource):
    def get(self, question_id):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT r.id AS id, r.question_id AS question_id, q.question AS question, "\
                            "qt.designation AS question_type_designation, r.user_id AS user_id, r.score AS score, "\
                            "r.comment AS comment, r.created_at AS created_at "\
                            "FROM result r "\
                            "JOIN question q ON r.question_id=q.id "\
                            "JOIN question_type qt ON q.question_type_id=qt.id "\
                            "WHERE question_id = " + str(question_id) + " "\
                            "ORDER BY r.id DESC")
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        if (user_exists(myresult)):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'data': data}
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': "User " + str(user_id) + " doesn't exist."}

        return jsonify(response)


# shows results filtered by question_id
class ResultByUserID(Resource):
    def get(self, user_id):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT r.id AS id, r.question_id AS question_id, q.question AS question, "\
                            "qt.designation AS question_type_designation, r.user_id AS user_id, r.score AS score, "\
                            "r.comment AS comment, r.created_at AS created_at "\
                            "FROM result r "\
                            "JOIN question q ON r.question_id=q.id "\
                            "JOIN question_type qt ON q.question_type_id=qt.id "\
                            "WHERE user_id = " + str(user_id) + " "\
                            "ORDER BY r.id DESC")
        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        if (user_exists(myresult)):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'data': data}
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': "User " + str(user_id) + " doesn't exist."}

        return jsonify(response)


#############################
# Visit
#############################
# VisitList
# shows a list of all Visits, and lets you POST to add new visit
class VisitList(Resource):
    def get(self):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT v.id AS id, v.user_id AS user_id, u.name AS name, "\
                            "u.email AS email, v.start_time AS start_time, "\
                            "v.end_time AS end_time, v.zone_id AS zone_id, z.designation AS zone_designation "\
                            "FROM visit v "\
                            "JOIN user u ON v.user_id=u.id "\
                            "JOIN zone z ON v.zone_id=z.id "\
                            "ORDER BY v.id DESC")

        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        response = {'success': True,
                    'timestamp': str(datetime.now()),
                    'data': data}

        return jsonify(response)

    # Adds new visit
    def post(self):
        data = request.get_json()
        
        missing_values = []

        if 'user_id' not in data: missing_values.append("user_id")
        if 'zone_id' not in data: missing_values.append("zone_id")
        
        insert_success = True
        error_message = ""

        if len(missing_values) > 0:
            insert_success = False
            error_message += "MISSING VALUES: " + ', '.join(missing_values)
        else:
            myconn = get_connection()
            mycursor = myconn.cursor()

            sql = "INSERT INTO visit (user_id, zone_id) VALUES (%s, %s)"
            val = (data['user_id'], data['zone_id'])

            insert_success = True
            error_message = ""

            try:
                mycursor.execute(sql, val)
                myconn.commit()
            except Exception as err:
                insert_success = False
                error_message += "MYSQL: {0}".format(err)


        if (insert_success):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'inserted_id': mycursor.lastrowid}
            logging.info("%s record(s) inserted.", mycursor.rowcount)
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': error_message}

        return jsonify(response)


class VisitPut(Resource):
    def put(self, visit_id):
        data = request.get_json()

        myconn = get_connection()
        mycursor = myconn.cursor()

        sql = "UPDATE visit SET end_time=now() WHERE id=%s"
        val = (visit_id,)

        insert_success = True
        error_message = ""

        try:
            mycursor.execute(sql, val)
            myconn.commit()
            # get updated visit data
            mycursor.execute("SELECT v.id AS id, v.user_id AS user_id, u.name AS name, "\
                                "u.email AS email, v.start_time AS start_time, "\
                                "v.end_time AS end_time, v.zone_id AS zone_id, z.designation AS zone_designation "\
                                "FROM visit v "\
                                "JOIN user u ON v.user_id=u.id "\
                                "JOIN zone z ON v.zone_id=z.id "\
                                "WHERE v.id = " + str(visit_id) + ";")

            myresult = mycursor.fetchall()

            row_headers=[x[0] for x in mycursor.description] #this will extract row headers

            response_data=[]
            for result in myresult:
                response_data.append(dict(zip(row_headers,result)))
        except Exception as err:
            insert_success = False
            error_message += "MYSQL: {0}".format(err)

        
        if (insert_success):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'result': "Updated " + str(mycursor.rowcount) + " row(s).",
                        'data': response_data}
            logging.info("%s record(s) updated.", mycursor.rowcount)
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': error_message}

        return jsonify(response)


class VisitByUserID(Resource):
    def get(self, user_id):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT v.id AS id, v.user_id AS user_id, u.name AS name, "\
                            "u.email AS email, v.start_time AS start_time, "\
                            "v.end_time AS end_time, v.zone_id AS zone_id, z.designation AS zone_designation "\
                            "FROM visit v "\
                            "JOIN user u ON v.user_id=u.id "\
                            "JOIN zone z ON v.zone_id=z.id;"\
                            "WHERE v.user_id=" + str(user_id) + ""\
                            "ORDER BY v.id DESC")

        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        response = {'success': True,
                    'timestamp': str(datetime.now()),
                    'data': data}

        return jsonify(response)


class VisitByZoneID(Resource):
    def get(self, zone_id):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT v.id AS id, v.user_id AS user_id, u.name AS name, "\
                            "u.email AS email, v.start_time AS start_time, "\
                            "v.end_time AS end_time, v.zone_id AS zone_id, z.designation AS zone_designation "\
                            "FROM visit v "\
                            "JOIN user u ON v.user_id=u.id "\
                            "JOIN zone z ON v.zone_id=z.id;"\
                            "WHERE v.zone_id=" + str(zone_id) + ""\
                            "ORDER BY v.id DESC")

        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        response = {'success': True,
                    'timestamp': str(datetime.now()),
                    'data': data}

        return jsonify(response)


#############################
# Beacon logs
#############################
# beacon logs list
# shows a list of all logs, and lets you POST to add new log
class BeaconLogList(Resource):
    def get(self):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT lb.id AS id, lb.beacon_id AS beacon_id, lb.enabled as enabled, lb.rssi AS rssi, lb.created_at "\
                            "FROM log_beacon lb "\
                            "ORDER BY lb.id DESC")

        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        response = {'success': True,
                    'timestamp': str(datetime.now()),
                    'data': data}

        return jsonify(response)

    # Adds new beacon_log
    def post(self):
        data = request.get_json()

        if 'rssi' in data:
            rssi = str(data['rssi'])
        else:
            rssi = None
        if 'enabled' in data:
            enabled = '1' if data['enabled'] else '0'
        else:
            enabled = '0'

        missing_values = []
        
        if 'minor' not in data: missing_values.append("minor")
        if 'major' not in data: missing_values.append("major")
        
        insert_success = True
        error_message = ""
        beacon_id = 0

        if len(missing_values) > 0:
            insert_success = False
            error_message += "MISSING VALUES: " + ', '.join(missing_values)
        else:
            myconn = get_connection()
            mycursor = myconn.cursor()

            try:
                
                beacon_id = getBeaconID(data['minor'], data['major'])

                if rssi is None:
                    sql = "INSERT INTO log_beacon (beacon_id, enabled) VALUES (%s, %s)"
                    val = (beacon_id, enabled)
                else:
                    sql = "INSERT INTO log_beacon (beacon_id, enabled, rssi) VALUES (%s, %s, %s)"
                    val = (beacon_id, '1', rssi)
                
                insert_success = True
                error_message = ""
            except:
                insert_success = False
                error_message = "Beacon doesn't exist."
            finally:
                try:
                    mycursor.execute(sql, val)
                    myconn.commit()
                    mycursor.close()
                except Exception as err:
                    insert_success = False
                    logging.info("Beacon doesn't exist.")
                    if error_message=="":
                        error_message = "MYSQL: {0}".format(err)


        if (insert_success):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'inserted_id': mycursor.lastrowid}
            logging.info("%s record(s) inserted.", mycursor.rowcount)
            #Starts a new visit if the previous one already finished and a beacon was detected
            #update_visit(beacon_id)
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': error_message}

        return jsonify(response)


class BeaconLogByBeaconID(Resource):
    def get(self, beacon_id):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT lb.id AS id, lb.beacon_id AS beacon_id, lb.enabled as enabled, lb.rssi AS rssi, lb.created_at "\
                            "FROM log_beacon lb "\
                            "WHERE lb.beacon_id=" + str(beacon_id) + " "\
                            "ORDER BY lb.id DESC")

        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        response = {'success': True,
                    'timestamp': str(datetime.now()),
                    'data': data}

        return jsonify(response)


#############################
# Kinect logs
#############################
# Kinect logs list
# shows a list of all logs, and lets you POST to add new log
class KinectLogList(Resource):
    def get(self):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT lb.id AS id, lb.visit_id AS visit_id, lb.data AS data, lb.created_at "\
                            "FROM log_kinect lb "\
                            "ORDER BY lb.id DESC")

        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        response = {'success': True,
                    'timestamp': str(datetime.now()),
                    'data': data}

        return jsonify(response)

    # Adds new kinect log
    def post(self):
        data = request.get_data()
        #data is received from processing like the following string: "some_value=some_value"
        data_str = str(data.decode("utf-8")).split('=')

        myconn = get_connection()
        mycursor = myconn.cursor()

        sql = "INSERT INTO log_kinect (visit_id, data) VALUES (%s, %s)"
        val = (int(data_str[0]), data_str[1])

        insert_success = True
        error_message = ""

        try:
            mycursor.execute(sql, val)
            myconn.commit()
        except Exception as err:
            insert_success = False
            error_message += "MYSQL: {0}".format(err)


        if (insert_success):
            response = {'success': True,
                        'timestamp': str(datetime.now()),
                        'inserted_id': mycursor.lastrowid}
            logging.info("%s record(s) inserted.", mycursor.rowcount)
        else:
            response = {'success': False,
                        'timestamp': str(datetime.now()),
                        'message': error_message}

        return jsonify(response)


class KinectLogByVisitID(Resource):
    def get(self, visit_id):
        myconn = get_connection()
        mycursor = myconn.cursor()
        mycursor.execute("SELECT lb.id AS id, lb.visit_id AS visit_id, lb.data AS data, lb.created_at "\
                            "FROM log_kinect lb "\
                            "WHERE lb.visit_id=" + str(visit_id) + " "\
                            "ORDER BY lb.id DESC")

        myresult = mycursor.fetchall()

        row_headers=[x[0] for x in mycursor.description] #this will extract row headers

        data=[]
        for result in myresult:
            data.append(dict(zip(row_headers,result)))

        response = {'success': True,
                    'timestamp': str(datetime.now()),
                    'data': data}

        return jsonify(response)


## Api resource routing here
api.add_resource(UserList, '/user')
api.add_resource(UserByID, '/user/<int:user_id>') # get user by id
api.add_resource(UserDonation, '/user/donation') # updates the user with the donation amount

api.add_resource(ResultList, '/result')
api.add_resource(ResultByQuestionID, '/result/question/<int:question_id>')
api.add_resource(ResultByUserID, '/result/user/<int:user_id>')

api.add_resource(VisitList, '/visit')
api.add_resource(VisitPut, '/visit/<int:visit_id>')
api.add_resource(VisitByUserID, '/visit/user/<int:user_id>')
api.add_resource(VisitByZoneID, '/visit/zone/<int:zone_id>')

api.add_resource(BeaconLogList, '/beacon_log')
api.add_resource(BeaconLogByBeaconID, '/beacon_log/beacon/<int:beacon_id>')

api.add_resource(KinectLogList, '/kinect_log')
api.add_resource(KinectLogByVisitID, '/kinect_log/visit/<int:visit_id>')

if __name__ == '__main__':
    logging.basicConfig(filename='log_api.log', filemode='w', format='%(asctime)s - %(message)s', level=logging.INFO)
    app.run(debug=True,host= '0.0.0.0')
