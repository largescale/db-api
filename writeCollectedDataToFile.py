import mysql.connector
import xlsxwriter
from datetime import datetime

connection_config = {
    'host':'localhost',
    'user':'app',
    'passwd':'SD2019app',
    'database':'dome'
}

mydb = mysql.connector.connect()

def get_connection():
    global mydb
    running = True
    while running:
        try:
            if (mydb.is_connected() is False): #If connection exists leave Try statement
                print("MYSQL: Trying to connect to the database.")
                mydb = mysql.connector.connect(**connection_config)
                print("MYSQL: Database connection was successfull")
        except Exception as err:
            print("MYSQL: {0}".format(err))
        finally:
            if mydb.is_connected() is True: #If connection exists leave Try statement
                running = False
                return mydb


#f= open("collected_data.txt","w+")
workbook = xlsxwriter.Workbook('collected_data.xlsx')
worksheet = workbook.add_worksheet()

user_id = 8
myconn = get_connection()
mycursor = myconn.cursor()
mycursor.execute("SELECT r.id AS id, r.question_id AS question_id, q.question AS question, "\
                            "qt.designation AS question_type_designation, r.user_id AS user_id, r.score AS score, "\
                            "r.comment AS comment, r.created_at AS created_at "\
                            "FROM result r "\
                            "JOIN question q ON r.question_id=q.id "\
                            "JOIN question_type qt ON q.question_type_id=qt.id "\
                            "WHERE user_id = " + str(user_id) + " "\
                            "ORDER BY r.question_id ASC")

myresult = mycursor.fetchall()

# Start from the first cell. Rows and columns are zero indexed.
row = 0
col = 0

last_question_id = 0

for result in myresult:
    print(result)

    if last_question_id == result[1]:
        row -= 1

    worksheet.write(row, col,     result[7].strftime('%Y-%m-%d %H:%M:%S')) # writes a readable timestamp
    worksheet.write(row, col + 1, result[1])
    worksheet.write(row, col + 2, result[2])
    worksheet.write(row, col + 3, result[5])
    worksheet.write(row, col + 4, result[6])
    
    last_question_id = result[1]
    row += 1
    #f.write(str(result[7]) + "-> QUESTION_ID:" + str(result[1]) + "    " + str(result[2]) + "    " + str(result[5]) + "    " + str(result[6]) + "\n")

workbook.close()

mycursor.close()
myconn.close()