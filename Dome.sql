-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 26, 2019 at 01:06 PM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dome`
--	
DROP DATABASE IF EXISTS `dome`;

CREATE DATABASE `dome`;
USE `dome`;

-- --------------------------------------------------------

--
-- Table structure for table `beacon`
--

DROP TABLE IF EXISTS `beacon`;
CREATE TABLE `beacon` (
  `id` int(11) NOT NULL,
  `identifier` text DEFAULT NULL,
  `uuid` varchar(100) NOT NULL,
  `minor` varchar(10) NOT NULL,
  `major` varchar(10) NOT NULL,
  `specie` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `active` tinyint(1) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `beacon`
--

INSERT INTO `beacon` (`id`, `identifier`, `uuid`, `minor`, `major`, `specie`, `created_at`, `active`, `last_updated`) VALUES
(1, NULL, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', '19469', '32531', 'Blowfish', '2019-06-14 15:12:17', 0, '2019-07-26 11:05:24'),
(2, NULL, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', '49640', '52087', 'Mero', '2019-06-14 15:12:17', 0, '2019-07-26 11:01:10'),
(3, NULL, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', '65182', '55208', 'Whale', '2019-06-14 15:12:17', 0, '2019-07-26 11:01:16'),
(4, NULL, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', '10230', '62739', 'Fangtooth', '2019-06-14 15:12:17', 0, '2019-07-26 10:58:08'),
(5, NULL, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', '20003', '65413', 'Dolphin', '2019-06-14 15:12:17', 0, '2019-07-26 11:06:46'),
(6, '4520a0b86b32cb8e35e3fd887f746f30', 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', '19248', '46544', 'NULL', '2019-07-18 09:46:12', 0, '2019-07-26 11:01:24'),
(7, 'f9b45a272f6d0f92c8618d2fc0866909', 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', '8460', '2366', 'NULL', '2019-07-18 09:48:31', 0, '2019-07-26 11:00:58'),
(8, 'e369b1ee4c6b5a2b112ff3ca2823d300', 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', '19297', '22494', 'NULL', '2019-07-18 09:57:46', 0, '2019-07-26 11:00:48');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

DROP TABLE IF EXISTS `gender`;
CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `designation` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `designation`) VALUES
(1, 'Male'),
(2, 'Female');

-- --------------------------------------------------------

--
-- Table structure for table `log_beacon`
--

DROP TABLE IF EXISTS `log_beacon`;
CREATE TABLE `log_beacon` (
  `id` int(11) UNSIGNED NOT NULL,
  `beacon_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `enabled` tinyint(4) NOT NULL,
  `rssi` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `log_kinect`
--

DROP TABLE IF EXISTS `log_kinect`;
CREATE TABLE `log_kinect` (
  `id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `data` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE `question` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `question_type_id` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `question`, `created_at`, `question_type_id`) VALUES
(1, 'I think I would like to use this app frequently.', '2019-07-05 11:47:00', 1),
(2, 'I found the app unnecessarily complex.', '2019-07-05 11:47:00', 1),
(3, 'I thought the app was easy to use.', '2019-07-05 11:47:00', 1),
(4, 'I think that I would need the support of a technical person to be able to use this app.', '2019-07-05 11:47:00', 1),
(5, 'I found the various functions in this app were well integrated.', '2019-07-05 11:47:00', 1),
(6, 'I thought there was too much inconsistency in this app.', '2019-07-05 11:47:00', 1),
(7, 'I would imagine that most people would learn to use this app very quickly.', '2019-07-05 11:47:01', 1),
(8, 'I found the app very cumbersome to use.', '2019-07-05 11:47:01', 1),
(9, 'I felt very confident using the app.', '2019-07-05 11:47:01', 1),
(10, 'I needed to learn a lot of things before I could get going with this app.', '2019-07-05 11:47:01', 1),
(11, 'Thanks! You just participated in measurements of the app usability. Next are measurements of your presence.', '2019-07-05 11:47:01', 1),
(12, 'How much were you able to control events?', '2019-07-05 11:47:01', 1),
(13, 'How responsive was the app to actions that you initiated (or performed)?', '2019-07-05 11:47:01', 1),
(14, 'How natural did your interactions with the app seem?', '2019-07-05 11:47:01', 1),
(15, 'How much did the visual aspects of the app involve you?', '2019-07-05 11:47:01', 1),
(16, 'How natural was the mechanism which controlled movement through the app?', '2019-07-05 11:47:01', 1),
(17, 'How compelling was your sense of objects moving through app?', '2019-07-05 11:47:01', 1),
(18, 'How much did your experiences in the app seem consistent with your real world experiences?', '2019-07-05 11:47:01', 1),
(19, 'Were you able to anticipate what would happen next in response to the actions that you performed?', '2019-07-05 11:47:01', 1),
(20, 'How completely were you able to actively survey or search the app using vision?', '2019-07-05 11:47:01', 1),
(21, 'How compelling was your sense of moving around inside the app?', '2019-07-05 11:47:01', 1),
(22, 'How closely were you able to examine objects?', '2019-07-05 11:47:01', 1),
(23, 'How well could you examine objects from multiple viewpoints?', '2019-07-05 11:47:01', 1),
(24, 'How involved were you in the app experience?', '2019-07-05 11:47:01', 1),
(25, 'How much delay did you experience between your actions and expected outcomes?', '2019-07-05 11:47:01', 1),
(26, 'How quickly did you adjust to the app experience?', '2019-07-05 11:47:01', 1),
(27, 'How proficient in moving and interacting with the app did you feel at the end of the experience?', '2019-07-05 11:47:01', 1),
(28, 'How much did the visual display quality interfere or distract you from performing assigned tasks or required activities?', '2019-07-05 11:47:01', 1),
(29, 'How much did the control devices interfere with the performance of assigned tasks or with other activities?', '2019-07-05 11:47:01', 1),
(30, 'How well could you concentrate on the assigned tasks or required activities rather than on the mechanisms used to perform those tasks or activities?', '2019-07-05 11:47:01', 1),
(31, 'How much did the auditory aspects of the environment involve you?', '2019-07-05 11:47:01', 1),
(32, 'How well could you identify sounds?', '2019-07-05 11:47:01', 1),
(33, 'How well could you localize sounds?', '2019-07-05 11:47:01', 1),
(34, 'How well could you actively survey or search the app using touch?', '2019-07-05 11:47:01', 1),
(35, 'How well could you move or manipulate objects in the app?', '2019-07-05 11:47:01', 1),
(36, 'Thanks! You just participated in measurements of app presence. Next are measurements of your cognitive load.', '2019-07-05 11:47:01', 1),
(37, 'While using the app, how mentally demanding was the task?', '2019-07-05 11:47:01', 1),
(38, 'While using the app, how physically demanding was the task?', '2019-07-05 11:47:01', 1),
(39, 'While using the app, how hurried or rushed was the pace of the task?', '2019-07-05 11:47:01', 1),
(40, 'While using the app, how successful were you in accomplishing what you were asked to do?', '2019-07-05 11:47:01', 1),
(41, 'While using the app, how hard did you have to work to accomplish your level of performance?', '2019-07-05 11:47:01', 1),
(42, 'While using the app, how insecure, discouraged, irritated, stressed, and annoyed wereyou?', '2019-07-05 11:47:01', 1),
(43, 'Thank you!\\nYou just participated in measurements of the app cognitive load. Next are measurements of your flow state.', '2019-07-05 11:47:02', 1),
(44, 'While using the app, I feel just the right amount of challenge.', '2019-07-05 11:47:02', 1),
(45, 'While using the app, my thoughts/activities run fluidly and smoothly.', '2019-07-05 11:47:02', 1),
(46, 'While using the app, I do not notice time passing.', '2019-07-05 11:47:02', 1),
(47, 'While using the app, I have no difficulty concentrating.', '2019-07-05 11:47:02', 1),
(48, 'While using the app, my mind is completely clear.', '2019-07-05 11:47:02', 1),
(49, 'While using the app, I am totally absorbed in what I am doing.', '2019-07-05 11:47:02', 1),
(50, 'While using the app, the right thoughts/movements occur of their own accord.', '2019-07-05 11:47:02', 1),
(51, 'While using the app, I know what I have to do each step of the way.', '2019-07-05 11:47:02', 1),
(52, 'While using the app, I feel that I have everything under control.', '2019-07-05 11:47:02', 1),
(53, 'While using the app, I am completely lost in thought.', '2019-07-05 11:47:02', 1),
(54, 'Compared to all other activities which I partake in, this one is easy.', '2019-07-05 11:47:02', 1),
(55, 'I think that my competence in this area is low.', '2019-07-05 11:47:02', 1),
(56, 'For me personally, the current demands are too low.', '2019-07-05 11:47:02', 1),
(57, 'Thank you!\\nYou just participated in measurements of the app flow state. Next are measurements of your intrinsic motivation.', '2019-07-05 11:47:02', 1),
(58, 'I enjoyed using this app very much.', '2019-07-05 11:47:02', 1),
(59, 'Using this app was fun to do.', '2019-07-05 11:47:02', 1),
(60, 'I thought this was a boring app.', '2019-07-05 11:47:02', 1),
(61, 'This app did not hold my attention at all.', '2019-07-05 11:47:02', 1),
(62, 'I would describe using this app as very interesting.', '2019-07-05 11:47:02', 1),
(63, 'I thought using this app was quite enjoyable.', '2019-07-05 11:47:02', 1),
(64, 'While I was using this app, I was thinking about how much I enjoyed it.', '2019-07-05 11:47:02', 1),
(65, 'I think I am pretty good at using this app.', '2019-07-05 11:47:02', 1),
(66, 'I think I did pretty well at using this app, compared to other persons.', '2019-07-05 11:47:02', 1),
(67, 'After working at using this app for awhile, I felt pretty competent.', '2019-07-05 11:47:02', 1),
(68, 'I am satisfied with my performance at using this app.', '2019-07-05 11:47:02', 1),
(69, 'I was pretty skilled at using this app.', '2019-07-05 11:47:02', 1),
(70, 'Using this app was an activity that I couldn’t do very well.', '2019-07-05 11:47:02', 1),
(71, 'I put a lot of effort into this.', '2019-07-05 11:47:02', 1),
(72, 'I didn’t try very hard to do well at using this app.', '2019-07-05 11:47:02', 1),
(73, 'I tried very hard on using this app.', '2019-07-05 11:47:02', 1),
(74, 'It was important to me to do well at using this app.', '2019-07-05 11:47:02', 1),
(75, 'I didn’t put much energy into using this app.', '2019-07-05 11:47:02', 1),
(76, 'I did not feel nervous at all while using this app.', '2019-07-05 11:47:02', 1),
(77, 'I felt very tense while using this app.', '2019-07-05 11:47:02', 1),
(78, 'I was very relaxed in using this app.', '2019-07-05 11:47:02', 1),
(79, 'I was anxious while working on this app.', '2019-07-05 11:47:02', 1),
(80, 'I felt pressured while doing tasks in this app.', '2019-07-05 11:47:02', 1),
(81, 'I believe I had some choice about doing this activity.', '2019-07-05 11:47:02', 1),
(82, 'I felt like it was not my own choice to do this task.', '2019-07-05 11:47:02', 1),
(83, 'I didn’t really have a choice about doing this task.', '2019-07-05 11:47:03', 1),
(84, 'I felt like I had to do this.', '2019-07-05 11:47:03', 1),
(85, 'I did this activity because I had no choice.', '2019-07-05 11:47:03', 1),
(86, 'I did this activity because I wanted to.', '2019-07-05 11:47:03', 1),
(87, 'I did this activity because I had to.', '2019-07-05 11:47:03', 1),
(88, 'I believe this activity could be of some value to me.', '2019-07-05 11:47:03', 1),
(89, 'I think that doing this activity is useful for marine biologists and tourists.', '2019-07-05 11:47:03', 1),
(90, 'I think this is important to do because it can raise awareness about ocean and cetacean concerns.', '2019-07-05 11:47:03', 1),
(91, 'I would be willing to do this again because it has some value to me.', '2019-07-05 11:47:03', 1),
(92, 'I think doing this activity could help me to become a sea citizen scientist.', '2019-07-05 11:47:03', 1),
(93, 'I believe doing this activity could be beneficial to me.', '2019-07-05 11:47:03', 1),
(94, 'I think this is an important activity.', '2019-07-05 11:47:03', 1),
(95, 'While using the app, I felt really distant to the spotted cetaceans.', '2019-07-05 11:47:03', 1),
(96, 'While using the app, I really doubt that spotted cetaceans and I would ever be friends.', '2019-07-05 11:47:03', 1),
(97, 'I felt like I could really trust this app.', '2019-07-05 11:47:03', 1),
(98, 'I’d like a chance to interact with this app more often.', '2019-07-05 11:47:03', 1),
(99, 'I’d really prefer not to interact with this app in the future.', '2019-07-05 11:47:03', 1),
(100, 'I don’t feel like I could really trust this app.', '2019-07-05 11:47:03', 1),
(101, 'While using the app, it is likely that the spotted cetaceans and I could become friends if we interacted a lot.', '2019-07-05 11:47:03', 1),
(102, 'While using the app, I feel close to the spotted cetaceans.', '2019-07-05 11:47:03', 1),
(103, 'Thank you!\\nYou just participated in measurements of the app intrinsic motivation. Here are some final short questions.', '2019-07-05 11:47:03', 1),
(104, 'Were you looking more to the actual whales and dolphins than the app?', '2019-07-05 11:47:03', 1),
(105, 'How important to you was the sound during the whale watching activity?', '2019-07-05 11:47:03', 1),
(106, 'Pick a word which describes the best your experience:', '2019-07-05 11:47:03', 1),
(107, 'How much did this app arouse you?', '2019-07-05 11:47:03', 1),
(108, 'How much did you find this app attractive?', '2019-07-05 11:47:03', 1),
(109, 'How much dominant you felt using this app?', '2019-07-05 11:47:03', 1),
(110, 'How much you liked reporting the sightings?', '2019-07-05 11:47:03', 1),
(111, 'How much you liked classifying the cetacean sounds?', '2019-07-05 11:47:03', 1),
(112, 'How much you liked gathering the cetacean photos?', '2019-07-05 11:47:03', 1),
(113, 'How much you would be willing to track the reported cetaceans from the shore?', '2019-07-05 11:47:03', 1),
(114, 'How much you would be willing to have the future updates of your spotted cetacean in your phone, during your daily life routine?', '2019-07-05 11:47:03', 1),
(115, 'How much you would be willing to adopt a digital version of the spotted cetacean?', '2019-07-05 11:47:03', 1),
(116, 'If you could, would you use your adopted cetacean as Tamagotchi?', '2019-07-05 11:47:03', 1),
(117, 'Please rate your previous experience with Tamagotchi:', '2019-07-05 11:47:03', 1),
(118, 'Please rate your previous experience as marine biologist:', '2019-07-05 11:47:03', 1),
(119, 'Please rate your previous experience as citizen scientist:', '2019-07-05 11:47:03', 1),
(120, 'Please rate your previous experience with green energy:', '2019-07-05 11:47:04', 1),
(121, 'During this experience, you used 100% solar energy, how much is this important to you?', '2019-07-05 11:47:04', 1),
(122, 'How much you liked/would you like to manipulate the collected images? E.g. pinch, zoom, rotate.', '2019-07-05 11:47:04', 1),
(123, 'How much you would like to know more about cetaceans\' vocal calls before the experience?', '2019-07-05 11:47:04', 1),
(124, 'While collecting an underwater photo, how much would you like to have an option to control the camera from the app to follow the cetacean?', '2019-07-05 11:47:04', 1),
(125, 'How much you would be interested in having an option of collecting the near real-time underwater 360 video of cetaceans?', '2019-07-05 11:47:04', 1),
(126, 'How much you would like to visit the place which offers the interactive/auugmented reality/virtual reality underwater 360 experience where you swim and dive with cetaceans?', '2019-07-05 11:47:04', 1),
(127, 'Add any additional comments', '2019-07-05 11:47:04', 1),
(128, 'Thank you for completing the feedback!\\n\\nYou will receive your results and collected media to your email address.', '2019-07-05 11:47:04', 1),
(129, 'CommentNameEmailCountryAgeGender', '2019-07-06 12:05:50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `question_type`
--

DROP TABLE IF EXISTS `question_type`;
CREATE TABLE `question_type` (
  `id` int(11) NOT NULL,
  `designation` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `question_type`
--

INSERT INTO `question_type` (`id`, `designation`) VALUES
(1, 'Default');

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
CREATE TABLE `result` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `comment` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender_id` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `country` varchar(45) NOT NULL,
  `donation` int(11) DEFAULT NULL,
  `mac_address` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO user(name, email, gender_id, birthday, country, mac_address) VALUES ('User1', 'user1@gmail.com', 1, '1999-07-10', 'Portugal', 'mac_address1');


-- --------------------------------------------------------

--
-- Table structure for table `visit`
--

DROP TABLE IF EXISTS `visit`;
CREATE TABLE `visit` (
  `id` int(11) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `end_time` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

DROP TABLE IF EXISTS `zone`;
CREATE TABLE `zone` (
  `id` int(11) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `beacon_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zone`
--

INSERT INTO `zone` (`id`, `designation`, `beacon_id`) VALUES
(1, 'Zone1', 2),
(2, 'Zone2', 1),
(3, 'Zone3', 3),
(4, 'Zone4', 4),
(5, 'Zone5', 5),
(6, 'Zone7', 6),
(7, 'Zone8', 7),
(8, 'Zone9', 8);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beacon`
--
ALTER TABLE `beacon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_beacon`
--
ALTER TABLE `log_beacon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_log_beacon_beacon1_idx` (`beacon_id`);

--
-- Indexes for table `log_kinect`
--
ALTER TABLE `log_kinect`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_visit_id_idx` (`visit_id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_question_question_type_idx` (`question_type_id`);

--
-- Indexes for table `question_type`
--
ALTER TABLE `question_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_result_question1_idx` (`question_id`),
  ADD KEY `fk_result_user1_idx` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_gender1_idx` (`gender_id`);

--
-- Indexes for table `visit`
--
ALTER TABLE `visit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_visit_user1_idx` (`user_id`),
  ADD KEY `fk_visit_zone1_idx` (`zone_id`);

--
-- Indexes for table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_zone_beacon1_idx` (`beacon_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beacon`
--
ALTER TABLE `beacon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `log_beacon`
--
ALTER TABLE `log_beacon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_kinect`
--
ALTER TABLE `log_kinect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `question_type`
--
ALTER TABLE `question_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `visit`
--
ALTER TABLE `visit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `zone`
--
ALTER TABLE `zone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `log_beacon`
--
ALTER TABLE `log_beacon`
  ADD CONSTRAINT `fk_log_beacon_beacon1` FOREIGN KEY (`beacon_id`) REFERENCES `beacon` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `log_kinect`
--
ALTER TABLE `log_kinect`
  ADD CONSTRAINT `fk_visit_id` FOREIGN KEY (`visit_id`) REFERENCES `visit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `fk_question_question_type` FOREIGN KEY (`question_type_id`) REFERENCES `question_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `result`
--
ALTER TABLE `result`
  ADD CONSTRAINT `fk_result_question1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_result_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_gender1` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `visit`
--
ALTER TABLE `visit`
  ADD CONSTRAINT `fk_visit_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_visit_zone1` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `zone`
--
ALTER TABLE `zone`
  ADD CONSTRAINT `fk_zone_beacon1` FOREIGN KEY (`beacon_id`) REFERENCES `beacon` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

DELIMITER $$
--
-- Events
--
DROP EVENT IF EXISTS `update_active_beacon_every_2sec`$$
CREATE DEFINER=`root`@`localhost` EVENT `update_active_beacon_every_2sec` ON SCHEDULE EVERY 2 SECOND STARTS '2019-07-26 10:13:52' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN

DECLARE beaconID INT;

select beacon_id into beaconID from log_beacon where created_at > date_sub(now(), interval 5 second) ORDER BY rssi DESC LIMIT 1;

if (beaconID is not NULL)
then
UPDATE beacon SET active = 0 WHERE id != beaconID;
UPDATE beacon SET active = 1 WHERE id = beaconID;
end if;

END$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
