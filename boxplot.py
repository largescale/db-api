import pandas as pd
import numpy as np
import csv

columns = []
values = []

# Getting data from the csv file "study_values.csv"
# The csv must only contain a first row with the column headers and the rest are values
# The values were copied from the Box plot charts sheet in the google sheets document
# Example: 
# | Q1 | Q2 | Q3 |
# |  3 |  2 |  5 |
# |  1 |  4 |  7 |
with open('study_values.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            line_count += 1
            for column in row:
                columns.append(column)
        
        current_row = []
        for cell in columns:
            if not row[cell] == '':
                current_row.append(int(row[cell]))
            else:
                current_row.append(row[cell])
        values.append(current_row)
        
        #print(f'\t{row["Q1"]} works in the {row["Q2"]} department, and was born in {row["Q3"]}.')


print(columns)

for i in range(len(values)):
    #remove empty values from array
    values[i] = [None if v is '' else v for v in values[i]]
    print(values[i])

df = pd.DataFrame(values, columns=columns)
#df = pd.DataFrame([[1,2,3],[1,2,3]], columns=['A','B','C'])
boxplot = df.boxplot(grid=False)#,figsize=(9,6)
boxplot.get_figure().savefig('boxplot.png')
