import xml.etree.ElementTree as ET
import requests
import mysql.connector
import re

connection_config = {
    'host':'192.168.0.117',
    'user':'app',
    'passwd':'SD2019app',
    'database':'dome'
}

mydb = mysql.connector.connect()

def get_connection():
    global mydb
    running = True
    while running:
        try:
            if (mydb.is_connected() is False): #If connection exists leave Try statement
                print("MYSQL: Trying to connect to the database.")
                mydb = mysql.connector.connect(**connection_config)
                print("MYSQL: Database connection was successfull")
        except Exception as err:
            print("MYSQL: {0}".format(err))
        finally:
            if mydb.is_connected() is True: #If connection exists leave Try statement
                running = False
                return mydb

def insertQuestionIntoDB(question):
    myconn = get_connection()
    mycursor = myconn.cursor()
    
    sql= "INSERT INTO question(question) VALUES ('" + escape_string(question) + "')"
    mycursor.execute(sql)

    mydb.commit()


def escape_string(text):
    escaped = text.translate(str.maketrans({"-":  r"\-",
                                          "]":  r"\]",
                                          "\\": r"\\",
                                          "^":  r"\^",
                                          "$":  r"\$",
                                          "*":  r"\*",
                                          ".":  r"\.",
                                          "'":  r"\'"}))
    return escaped


def writeQuestionToFile(question):
    f = open("questions.txt", "a")
    f.write(question + "\n")
    f.close()


tree = ET.parse('strings.xml')
root = tree.getroot()

for child in root:
    #if the tag is a questions then:
    if (child.attrib['name'][0] == 'q' and child.attrib['name'][1] != 'u' and child.attrib['name'] != 'q0'):
        print(child.text)
        #insertQuestionIntoDB(child.text)
        writeQuestionToFile(child.text)
